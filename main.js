//1. fundamental methods
//____________________________________________________________
//1) Document 객체의 method  : getElementsByTagName()
//전체 문서에서 태그 이름이 div 인 엘리먼트 찾기
//var divs = window.document.getElementsByTagName('div');
//alert("문서 내의 div 엘리먼트 개수 : "+ divs.length);

/*
// 1-변형.
//모든 div 엘리먼트 테두리 파랗게 하기
var divs = window.document.getElementsByTagName('div');
for(var i=0;i<divs.length;i++) {
    //찾은 노드에서 n 번째 노드에 접근해
    var div= divs.item(i);
    //style 변경하기
    div.style.border = "1px solid #03c";
}
//*/

/* 
//2) Element 객체의 method : getElementsByTagName()
//세번째 div 엘리먼트의 자식 div 엘리먼트 테두리 초록색으로 하기
var divs = window.document.getElementsByTagName('div');
var div2 = divs[2];
var div2Child = div2.getElementsByTagName("div");
for(var i=0; i<div2Child.length;i++) {
    div2Child[i].style.border="2px solid #090";
}
//*/

//3) Document 객체의 method : getElementsByClassName()
//class 가 content_data 인 elements 테두리 빨갛게
/*
var contentData = window.document.getElementsByClassName('content_data');
for(var i=0;i<contentData.length;i++) {
    contentData[i].style.border="2px solid #c33";
}
//*/

//4) Document 객체 method : getElementById()
//id 가 header 인 element 테두리 빨갛게
/*
var header = window.document.getElementById("header")
header.style.border="2px solid #c33";
//*/

//_______end of 1.fundamental methods________________________

//2. related nodes (child, parent, siblings)
//____________________________________________________________

//1) Node method : childNodes,previousSibling, nextSibling
//id 가 sample_page 인 엘리먼트의 자식을 찾는다. 
/*
var page = window.document.getElementById("sample_page");
var nodes = page.childNodes;
//childNodes 는 NodeList 객체이다.

alert('#sample_page의 자식 개수는 '+nodes.length);
for(var i=0;i<nodes.length;i++) {
    if(nodes[i].nodeType==1) {
        nodes[i].style.border="4px solid #f00";
    }
}
//div 앞뒤에는 개행문자로 된 텍스트 노드가 꼭 끼어있다. 
//lastChild 와 firstChild 는 우리가 보이는 가장 처음과 마지막의 각각 밖에 있는 텍스트 노드이다. 
// 그래서 한칸 더 땡겨 와야 우리가 고르고 싶은 노드가 선택된다.
page.lastChild.previousSibling.style.color="#0f0";
page.firstChild.nextSibling.style.color="#0f0";
//*/

//_______end of 2.related Nodes________________________

//3. creating & deleting Nodes
//____________________________________________________________
//1) 한 엘리먼트의 앞에 집어넣기 -createElement(), appendChild(), insertBefore() 사용
/*
var page = window.document.getElementById("sample_page");

var p1 = document.createElement("p");
var text1 = document.createTextNode("추가한 내용1");
p1.appendChild(text1);
p1.style.border="2px solid #ed9";

//insertBefore 의 두번째 인수는 한 element 또는 그 id 를 넣으면 된다.
page.insertBefore(p1,page.firstChild.nextSibling);
//*/

//2) 한 엘리먼트의 가장 마지막 자식으로 넣기 - appendChild()
/*
var page = window.document.getElementById("sample_page");

var p2 = document.createElement("p");
var text2 = document.createTextNode("가장 마지막 자식");
p2.appendChild(text2);
p2.style.border="2px solid #ed9";

page.appendChild(p2);
//*/

//3) innerHTML 사용
/*
var page = window.document.getElementById("sample_page");

var div1 = document.createElement("div");
div1.style.border="2px solid #ed9";
//innerHTML 은 HTML 형식을 그냥 string 으로 쓰면 된다. 
div1.innerHTML="<p>innerHTML로 생성한 div(p)</p>";

page.insertBefore(div1,content);
//*/

//4) element 지우기 - removeChild()
/*
var page = window.document.getElementById("sample_page");

page.removeChild(header);
//*/

//5) textNode 내용 바꾸기 - nodeValue
/*
var page = window.document.getElementById("sample_page");
var firstText = page.firstChild;
firstText.nodeValue ="textNode 내용이 바뀝니다. (div, id=sample_page, class=page)";
//*/

//_______end of 3.creating & deleting Nodes________________________

//4. managing attributes
//____________________________________________________________










//_______end of 4. managing attributes________________________
