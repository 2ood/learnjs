#### 2021.6.6 TIL ([git](https://github.com/2ood/learnjs/tree/fac4510fa0ec870a605f2f5cddd69d048f5260e9))

1. Node, Document, Element, HTMLDocument, HTMLElement

2. getElementsByTagName(), getElementsByClassName(), getElementByID()


#### 2021.6.6 TIL ([git](https://gitlab.com/2ood/learnjs/-/tree/e41ecaff0ef983ece2863c43bd11261f48b78a59))

1. childNodes, nextSibling, previousSbiling, parentNode

2. createElement(), appendChild() insertBefore(), nodeValue, removeChild()

3. style, attributes, getAttribute(), getComputedStyle(), removeProperty(), removeNamedItem()
